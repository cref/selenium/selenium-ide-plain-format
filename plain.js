/**
 * How to use:
 * Selenium IDE -> Options -> Options... -> Formats -> Add
 * Name of the format: Plain
 * Source: Copy and paste the contents of this file
 * 
 * TODO: verify whether comments will be correctly parsed and formatted.
*/

/**
 * Parse source and update TestCase. Throw an exception if any error occurs.
 *
 * @param testCase TestCase to update
 * @param source The source to parse
 */
function parse(testCase, source) {
  testCase.setCommands(source.split("\n").map(function (line) {
  	if (line[0] === "#") return new Comment(line.substr(1));
  	var command = new Command(), args = line.split("\t");
	command.command = args[0];
	command.target = args[1];
	command.value = args[2];
  	return command;
  }));
}
/**
 * Format TestCase and return the source.
 *
 * @param testCase TestCase to format
 * @param name The name of the test case, if any. It may be used to embed title into the source.
 */
function format(testCase, name) {
  return formatCommands(testCase.commands);
}

/**
 * Format an array of commands to the snippet of source.
 * Used to copy the source into the clipboard.
 *
 * @param commands The array of commands to sort.
 */
function formatCommands(commands) {
	return commands.map(function (step) {
		return step.type === "comment"
			? "#" + step.comment
			: [step.command, step.target, step.value].join("\t");
	}).join("\n");
}
