# selenium-ide-plain-format

This format for Selenium IDE uses a plain text format in which each line represents an action.
The command, target and value are seperated with a tab character.
See comments in Plain.js for usage.

The advantages of the Plain format:

1. Easily parsable
2. SCM diff friendly
3. Easy to read, write and / or modify manually using a text editor, great for TDD, BDD, Specification by Example
